import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from "../Components/Login";
import Residence from "../Components/Residence";
import ResidenceInfo from "../Components/ResidenceInfo";
import Subspace from "../Components/Subspace";
import PresentList from "../Components/PresentList";

const AppStack = createStackNavigator(
    {
        Residence: {
            screen: Residence,
            navigationOptions: {
                title: 'Liste des résidences'
            }
        },
        ResidenceInfo: {
            screen: ResidenceInfo,
            navigationOptions: {
                title: 'Information sur la Résidence'
            }
        },
        Subspace: {
            screen: Subspace,
            navigationOptions: {
                title: 'Ajouter des informations'
            }
        },
        PresentList: {
            screen: PresentList,
            navigationOptions: {
                title: 'Modifier la liste des présents'
            }
        }
    });
const AuthStack = createStackNavigator(
    {
        Login: {
            screen: Login,
            navigationOptions: {
                title: 'Connexion'
            }
        }
    });

export default createAppContainer(createSwitchNavigator(
    {
        App: AppStack,
        Auth: AuthStack
    },
    {
        initialRouteName: 'Auth',
    }
));