import React from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import styles from '../Styles/residenceInfoItem';
import {Icon} from "react-native-elements";
import {connect} from 'react-redux';

class ResidenceInfoItem extends React.Component {

    render() {
        const { space, loadSubspace } = this.props;
        return (
            <View>
                <View style={styles.spaceTitleContainer}>
                    <Text style={styles.spaceTitle}>{space.nom.toUpperCase()}</Text>
                </View>
                <FlatList style={styles.listContainer}
                          data={space.sousEspace}
                          keyExtractor={(item) => item.nom.toString()}
                          renderItem={({item}) =>
                              <TouchableOpacity style={styles.subspace} onPress={() => loadSubspace(item, space.nom)}>
                                  <Text style={styles.subspaceName}>{item.nom}</Text>
                                  <View style={styles.subspaceIconContainer}>
                                      <Icon name="chevron-right" size={40} color="#000" />
                                  </View>
                              </TouchableOpacity>
                          }
                />
            </View>
        );
    }
}

export default connect(state => state)(ResidenceInfoItem);