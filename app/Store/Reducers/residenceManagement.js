const initialState = { residence: {}, presentList: [] };

function residenceManagement(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'ADD_RESIDENCE':
            nextState = {
                ...state,
                residence: action.value
            };
            return nextState || state;
        case 'UPDATE_PRESENT_LIST':
            nextState = {
                ...state,
                presentList: action.value
            };
            return nextState || state;
        default:
            return state;
    }
}

export default residenceManagement;