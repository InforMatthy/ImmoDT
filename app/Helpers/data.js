export default data = [
    {
        "nom":"Les Berges",
        "idResidence":20,
        "adresse":"2-4-6 avenue de Karben",
        "codePostal":31520,
        "ville":"Ramonville St Agne",
        "gestionnaire":"syndic1@immodt.com",
        "derniereVisite":"2019-08-17T03:24:00",
        "membresCS":[
            {
                "titre":"Madame",
                "nom":"Reulet",
                "prenom":"Corinne"
            },
            {
                "titre":"Monsieur",
                "nom":"Cros",
                "prenom":""
            }
        ],
        "espaces":[
            {
                "nom": "Batiment 2",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    }
                ]
            },
            {
                "nom": "Batiment 4",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    }
                ]
            },
            {
                "nom": "Batiment 6",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    }
                ]
            },
            {
                "nom": "Exterieur",
                "sousEspace":[
                    {
                        "nom": "Boite aux lettres"
                    },
                    {
                        "nom": "Parkings"
                    },
                    {
                        "nom": "Portail"
                    }
                ]
            }
        ]
    },
    {
        "nom":"Les Toits Tolosan",
        "idResidence":145,
        "adresse":"145 chemin de Tournefeuille",
        "codePostal":31300,
        "ville":"Toulouse",
        "gestionnaire":"syndic1@immodt.com",
        "derniereVisite":"2019-08-17T03:24:00",
        "membresCS":[
            {
                "titre":"Madame",
                "nom":"Lourenco",
                "prenom":"Marie Francoise"
            },
            {
                "titre":"Monsieur",
                "nom":"Duberge",
                "prenom":""
            }
        ],
        "espaces":[
            {
                "nom": "Batiment A",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Batiment B",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Batiment C",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Batiment D",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Batiment E",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Batiment F",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Ascenseur"
                    },
                    {
                        "nom": "Sous sol"
                    }
                ]
            },
            {
                "nom": "Exterieur",
                "sousEspace":[
                    {
                        "nom": "Piscine"
                    },
                    {
                        "nom": "Boites aux lettres"
                    },
                    {
                        "nom": "Espaces Verts"
                    }
                ]
            },
            {
                "nom": "Autres",
                "sousEspace":[
                    {
                        "nom": "Chaufferie"
                    },
                    {
                        "nom": "Portails"
                    }
                ]
            }
        ]
    },
    {
        "nom":"Evergreen",
        "idResidence":860,
        "adresse":"86 route d'Agde",
        "codePostal":31500,
        "ville":"Toulouse",
        "gestionnaire":"syndic1@immodt.com",
        "derniereVisite":"2019-08-17T03:24:00",
        "membresCS":[
            {
                "titre":"Madame",
                "nom":"Duverneuil",
                "prenom":""
            },
            {
                "titre":"Monsieur",
                "nom":"Bernard",
                "prenom":"Florian"
            },
            {
                "titre":"Monsieur",
                "nom":"Pierre",
                "prenom":""
            }
        ],
        "espaces":[
            {
                "nom": "Batiment",
                "sousEspace":[
                    {
                        "nom": "RDC"
                    },
                    {
                        "nom": "Etages"
                    },
                    {
                        "nom": "Parking sous sol"
                    },
                    {
                        "nom": "Toitures végétalisées"
                    },
                    {
                        "nom": "Chaufferie"
                    }
                ]
            },
            {
                "nom": "Exterieur",
                "sousEspace":[
                    {
                        "nom": "Espaces Verts"
                    },
                    {
                        "nom": "Portails"
                    },
                    {
                        "nom": "Aire de présentation"
                    }
                ]
            }
        ]
    }
];