import React from 'react';
import {View, FlatList, Alert, ActivityIndicator, TouchableOpacity, ToastAndroid, Image} from 'react-native';
import ResidenceItem from './ResidenceItem';
import {connect} from 'react-redux';
import {getResidences} from '../API/api';

import styles from '../Styles/residence';
import { Icon } from 'react-native-elements';

class Residence extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            residences: [],
            isLoading: false,
            isReloaded: false
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({ onRefresh: this._onRefresh });
        const action = {
            type: "ADD_TOKEN",
            value: {
                token: this.props.navigation.state.params.token
            }
        };
        this.props.dispatch(action);
        this.setState({ isLoading: true });
        setTimeout(() => this._loadResidence(), 500);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <TouchableOpacity onPress={
                    () => navigation.getParam('onRefresh')()
                }>
                    <Icon name="refresh" size={30} color="rgba(0, 0, 0, 0.6)"/>
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity onPress={
                    () => {
                        Alert.alert(
                            'Déconnexion',
                            'Voulez-vous vraiment vous déconnecter ?',
                            [
                                {
                                    text: 'Oui',
                                    onPress: () => {
                                        navigation.navigate('Login');
                                        ToastAndroid.show('Vous vous êtes déconnecté.', ToastAndroid.LONG);
                                    }
                                },
                                {
                                    text: 'Annuler',
                                    style: 'cancel',
                                }
                            ]
                        );
                    }
                }>
                    <Image source={require('../../assets/logo_icon_logout.png')} style={{width: 30, height: 30, opacity: 0.6}}/>
                </TouchableOpacity>
            )
        }
    };

    _onRefresh = () => {
        this.setState({
            isLoading: true,
            residences: [],
            isReloaded: true
        }, () => {
            this._loadResidence();
        });
    };

    _loadResidence() {
        this.setState({ isLoading: true });
        getResidences(this.props.token).then((data) => {
            this.setState({
                residences: data,
                isLoading: false
            });
        });
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    _confirmForVisiting = (residence, isReloaded) => {
        Alert.alert(
            'Nouvelle Visite',
            'Commencer la visite de "' + residence.nom  + '" ?\n' +
            'Toutes vos données seront sauvegardées sur votre téléphone tant que vous n\'aurez pas mis fin à la visite en appuyant sur ' +
            'le bouton retour, ou sur le bouton "Terminer la visite".\n' +
            'Si l\'application venait à se fermer, il suffit de la relancer, se reconnecter, et relancer la visite pour reprendre là où ' +
            'vous en étiez.',
            [
                {
                    text: 'Oui',
                    onPress: () => this.props.navigation.navigate('ResidenceInfo', { residence: residence, isReloaded: isReloaded })
                },
                {
                    text: 'Annuler',
                    style: 'cancel',
                }
            ]
        );
    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <FlatList style={styles.itemContainer}
                    data={this.state.residences}
                    keyExtractor={(item) => item.idResidence.toString()}
                    renderItem={
                        ({item}) => <ResidenceItem
                            residence={item}
                            confirmForVisiting={this._confirmForVisiting}
                            isReloaded={this.state.isReloaded}
                        />
                    }
                />
                {this._displayLoading()}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.tokenManagement.token
    };
};

export default connect(mapStateToProps)(Residence);