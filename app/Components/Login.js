import React from 'react';
import {
    View,
    TextInput,
    Text,
    Image,
    Alert,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {getUserToken} from '../API/api';

import styles from '../Styles/login';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mail: '',
            password: '',
            isLoading: false
        };
    }

    static navigationOptions = {
        header: null
    };

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    checkLogin() {
        this.setState({ isLoading: true });

        // TODO FOR DEV ONLY
        const { mail, pass } = this.state;
        /*const mail = "dev@nanokwant.com",
              pass = "immoapp"; /* */
        getUserToken(mail, pass).then((data) => {
            if (data.accessToken) {
                this.props.navigation.navigate('Residence', { token: data });
            } else {
                Alert.alert('Erreur', 'Email ou mot de passe invalide.', [{
                    text: 'Ok'
                }]);
                this.setState({isLoading: false});
            }
        }).catch(() => {
            Alert.alert('Erreur', 'Veuillez vérifier votre connexion internet.', [{
                text: 'Ok'
            }]);
            this.setState({isLoading: false});
        });
    }

    render() {
        const logoSource =  require('../../assets/logo_visite_immo.png');

        return (
            <KeyboardAwareScrollView>
                <View style={styles.mainContainer}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} source={logoSource}/>
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>Identification</Text>
                    </View>

                    <View style={styles.inputContainer}>
                        <View>
                            <Text style={styles.inputText}>Email</Text>
                        </View>
                        <View style={styles.inputSection}>
                            <TextInput style={styles.textInput}
                                       placeholder={'utilisateur@mail.fr'}
                                       onChangeText={text => this.setState({ mail: text})}
                            />
                            <Icon name="email" size={20} color="#000"/>
                        </View>

                        <View>
                            <Text style={styles.inputText}>Mot de passe</Text>
                        </View>
                        <View style={styles.inputSection}>
                            <TextInput style={styles.textInput}
                                       placeholder={'*****'}
                                       secureTextEntry={true}
                                       onChangeText={text => this.setState({pass: text})}
                            />
                            <Icon name="lock" size={20} color="#000"/>
                        </View>
                        <TouchableOpacity style={styles.button} onPress={() => this.checkLogin()}>
                            <Text style={styles.buttonText}>Connexion</Text>
                        </TouchableOpacity>
                    </View>
                    {this._displayLoading()}
                </View>
            </KeyboardAwareScrollView>

        );
    }
}

export default Login;