import { createStore  } from "redux";
import tokenManagement from "./Reducers/tokenManagement";
import residenceManagement from "./Reducers/residenceManagement";
import subspaceManagement from "./Reducers/subspaceManagement";
import { persistCombineReducers } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';

const rootPersistConfig = {
    key: 'root',
    storage: AsyncStorage
};

export default createStore(persistCombineReducers(rootPersistConfig, {tokenManagement, residenceManagement, subspaceManagement}))