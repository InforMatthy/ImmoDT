import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        fontFamily: 'Roboto',
        minHeight: 150,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        backgroundColor: 'white',
        marginTop: 5,
        marginRight: 5,
        marginLeft: 5,
        marginBottom: 5,
        paddingTop: 10,
        paddingRight: 15,
        paddingLeft: 15,
        paddingBottom: 10
    },
    title: {
        color: '#FFF',
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        marginBottom: 5,
        borderRadius: 2,
        backgroundColor: "rgba(0, 51, 153, 0.5)"
    },
    address: {
        paddingLeft: 20,
        fontSize: 16
    },
    lastVisit: {
        textAlign: 'right',
        fontSize: 14,
        marginTop: 10
    }
});

export default styles;