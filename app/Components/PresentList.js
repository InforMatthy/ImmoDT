import React from 'react';
import {FlatList, Picker, Text, TextInput, ToastAndroid, TouchableOpacity, View} from 'react-native';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {connect} from 'react-redux';

import styles from '../Styles/presentList';
import {Icon, Input} from "react-native-elements";

class PresentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presentList: this.props.presentList ? this.props.presentList : this.props.residence.membresCS,
            addingNewPresent: false,
            genderInTemp: 'Monsieur',
            nameInTemp: '',
            firstNameInTemp: ''
        };
        this._deletePresent = this._deletePresent.bind(this);
    }

    _addingPresent() {
        if (!this.state.addingNewPresent) {
            let nextState = this.state.presentList;
            nextState.push({_id: nextState.length, nom: '', prenom: '', newCreate: true});
            this.setState({presentList: nextState, addingNewPresent: true});
        } else {
            ToastAndroid.show('Vous devez valider la liste avant de pouvoir en ajouter un autre.', ToastAndroid.SHORT);
        }
    }

    _displayGenrePicker(titre) {
        return (
            <Picker style={styles.textInput}
                selectedValue={titre}
                onValueChange={(itemValue, itemIndex) => this.setState({genderInTemp: itemValue})}>
                <Picker.Item label="Monsieur" value="Monsieur" />
                <Picker.Item label="Madame" value="Madame" />
                <Picker.Item label="Société" value="Société" />
            </Picker>
        );
    }

    _updateResidence() {
        let nextResidence = this.props.residence;
        nextResidence.membresCS = this.state.presentList;
        let action = {
            type: 'ADD_RESIDENCE',
            value: nextResidence
        };
        this.props.dispatch(action);
    }

    _deletePresent(deleteItem) {
        let nextPresent = this.state.presentList;
        nextPresent = nextPresent.filter((item) => deleteItem._id !== item._id);
        this.setState({presentList: nextPresent}, () => {
            this._updateResidence();
        });
        ToastAndroid.show('Suppression effectué avec succès', ToastAndroid.SHORT);
    }

    _updateNewPresent() {
        let last = this.state.presentList.length - 1,
            nextState = this.state.presentList;
        let nextPresentList = {
            _id: last,
            titre: this.state.genderInTemp,
            nom: this.state.nameInTemp,
            prenom: this.state.firstNameInTemp
        };
        nextState[last] = nextPresentList;
        this.setState({
            presentList: nextState,
            addingNewPresent: false,
            genderInTemp: 'Monsieur',
            nameInTemp: '',
            firstNameInTemp: ''
        });
        this._updateResidence();
    }

    _displayPresentInput = ({item}) => {
        if (item.newCreate) {
            return (
                <View style={styles.inputGroup}>
                    <View style={styles.inputContainer}>
                        {this._displayGenrePicker(this.state.genderInTemp)}
                        <View style={styles.textInputGroup}>
                            <Text>Nom : </Text>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.setState({nameInTemp: text})}
                            />
                        </View>
                        <View style={styles.textInputGroup}>
                            <Text>Prénom : </Text>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.setState({firstNameInTemp: text})}
                            />
                        </View>
                    </View>
                    <TouchableOpacity style={styles.validateOption} onPress={this._updateNewPresent.bind(this)}>
                        <Icon name="check" size={30} color="#FFF"/>
                    </TouchableOpacity>
                </View>
            );
        } else {
            const nameDisplay = (item._id || item.titre ? item.titre + ' ' + (item.prenom ? item.prenom : '') + ' ' + item.nom : '');
            return (
                <View style={styles.inputContainerValidate}>
                    <TextInput style={styles.textInputValidate}
                               value={nameDisplay}
                               editable={false}
                    />
                    <TouchableOpacity style={styles.deleteOption} onPress={() => this._deletePresent(item)}>
                        <Icon name="delete" size={30} color="#FFF"/>
                    </TouchableOpacity>
                </View>
            );
        }
    };

    render() {
        return (
            <KeyboardAwareScrollView>
                <View style={styles.mainContainer}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>
                            Liste des présents
                        </Text>
                        <TouchableOpacity onPress={this._addingPresent.bind(this)}>
                            <Icon name='add' size={30} color='#FFF' />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.secondContainer}>
                        <FlatList
                            data={this.state.presentList}
                            keyExtractor={item => item._id}
                            renderItem={this._displayPresentInput}
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        residence: state.residenceManagement.residence
    };
};

export default connect(mapStateToProps)(PresentList);