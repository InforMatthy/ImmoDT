const initialState = { picturesAndComment: [] };

function subspaceManagement(state = initialState, action) {
    let nextState;

    switch (action.type) {
        case 'ADD_PICTURES_AND_COMMENT':
            const idSubspace = state.picturesAndComment.findIndex(item => item.idSubspace === action.value.idSubspace);
            if (idSubspace !== -1) {
                // We have the residence and subspace in state
                let temp = state.picturesAndComment.find(item => item.idSubspace === action.value.idSubspace);
                temp.pictures = action.value.pictures;
                temp.comment = action.value.comment;
                nextState = {
                    ...state,
                    picturesAndComment: [
                        ...state.picturesAndComment.filter( (item, index) => index !== idSubspace),
                        temp
                    ]
                };
            } else {
                // We haven't the residence and subspace in state
                nextState = {
                    ...state,
                    picturesAndComment: [
                        ...state.picturesAndComment,
                        {
                            idSubspace: action.value.idSubspace,
                            pictures: action.value.pictures,
                            comment: action.value.comment
                        }
                    ]
                };
            }
            return nextState || state;
        case 'DELETE_ALL':
            return initialState;
        default:
            return state;
    }
}

export default subspaceManagement;