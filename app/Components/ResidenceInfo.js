import React from 'react';
import {
    View,
    Text,
    BackHandler,
    ToastAndroid,
    TouchableOpacity,
    FlatList,
    Alert,
    ActivityIndicator
} from 'react-native';
import { HeaderBackButton } from "react-navigation-stack";
import {Icon} from "react-native-elements";
import {connect} from 'react-redux';
import {postVisit, postPhoto, patchResidence} from '../API/api';

import ResidenceInfoItem from "./ResidenceInfoItem";
import styles from '../Styles/residenceInfo';
import stylesItem from '../Styles/residenceInfoItem';

class ResidenceInfo extends React.Component {

    static isSaved = false;

    /**
     * Secure the navigation from menu button pressed
     * @param navigation
     * @returns {{headerLeft: *}}
     */
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: (
                <HeaderBackButton onPress={
                    () => {
                        if (!this.isSaved) {
                            Alert.alert(
                                'Avertissement',
                                'Êtes-vous sûr de vouloir quitter cette visite sans sauvegarder ?',
                                [
                                    {
                                        text: 'Quitter',
                                        onPress: () => {
                                            navigation.getParam('doDeleteAll')();
                                            navigation.goBack();
                                        }
                                    },
                                    {
                                        text: ' Continuer',
                                        style: 'cancel',
                                    }
                                ]
                            );
                        } else {
                            navigation.goBack();
                        }
                    }
                } />
            )
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            allSaved: false
        };
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    _doDeleteAll = (text = '') => {
        this.props.dispatch({
            type: 'DELETE_ALL',
            value: {
                idSubspace: ''
            }
        });
        if (text !== 'endingUpload') {
            ToastAndroid.show('Toutes les informations liées à la visite ont été effacées.', ToastAndroid.LONG);
        }
    };

    /**
     * 3 method for handle back button pressed
     */
    componentDidMount() {
        const residence = this.props.navigation.state.params.residence,
            isReloaded = this.props.navigation.state.params.isReloaded;

        if (isReloaded || this.props.residence._id !== residence._id) {
            const action = {
                type: "ADD_RESIDENCE",
                value: residence
            };
            this.props.dispatch(action);
        }
        this.props.navigation.setParams({ doDeleteAll: this._doDeleteAll });
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        ToastAndroid.show('Le bouton retour est désactivé pendant une visite', ToastAndroid.SHORT);
        return true;
    }

    _loadSubspace = (subspace, spaceName) => {
        this.props.navigation.navigate('Subspace', {
            subspace: subspace,
            residence: this.props.residence,
            spaceName: spaceName.toUpperCase()
        });
    };

    _loadPresentList = () => {
        this.props.navigation.navigate('PresentList', {
            residence: this.props.residence
        });
    };

    _endingVisit() {
        Alert.alert(
            'Terminer la visite',
            'Vous allez mettre fin à votre visite, toutes les données seront sauvegardées. \n' +
            'Veuillez vérifier que vous êtes bien connecté(e) à Internet.',
            [
                {
                    text: 'Terminer la visite',
                    onPress: () => this._saveAllData()
                },
                {
                    text: 'Annuler',
                    style: 'cancel',
                }
            ]
        );
    };

    _saveAllData() {
        // Création des variables pour lancer la sauvegarde.
        const token = this.props.token;
        let data = this.props.picturesAndComment;
        let date = new Date();

        // Modification et ajout des membres pour envoie à la DB
        let membresCS = this.props.residence.membresCS;
        membresCS.forEach((item) => {
            delete item._id;
        });

        // Création de la variable de mise en forme pour sauvegarder dans la BDD
        let newData = {
            residenceId: this.props.residence._id,
            date: date,
            espaces: [],
            presents: membresCS
        };

        // Mise en forme des variables pour envoie.
        data.forEach((item, i) => {
            newData.espaces[i] = {
                espaceId: item.idSubspace,
                photosPath: [],
                comment: item.comment
            };

            item.pictures.forEach((item2, j) => {
                newData.espaces[i].photosPath[j] = item2.name;
            });
        });


        // Préparation du tableau avec les différentes requêtes
        let picturesDataPromise = [
            postVisit(token, newData)
        ];


        // Parcours des données pour préparation des requêtes images par images
        data.forEach((item) => {
            if (item.pictures[0] && item.pictures[0].name) {
                item.pictures.forEach((item2) => {
                    // Mise en place de la valeur d'envoi pour chaque image.
                    let picturesDataForUpload = {
                        idResidence: this.props.residence._id,
                        idSubspace: item.idSubspace,
                        value: new FormData()
                    };
                    // Mise en forme des valeurs de l'image pour envoie
                    picturesDataForUpload.value.append(item2.name, {
                        uri: item2.uri,
                        type: "image/jpeg",
                        name: item2.name
                    });
                    picturesDataPromise.push(postPhoto(token, picturesDataForUpload));
                });
            }
        });

        this.setState({ isLoading: true });
        ToastAndroid.show('Enregistrement en cours, veuillez patienter...', ToastAndroid.LONG);

        Promise.all(picturesDataPromise).then((arrayOfResponses) => {
            console.log(JSON.stringify(arrayOfResponses, null, 4));
            this.setState({ isLoading: false });
            ResidenceInfo.isSaved = true;
            ToastAndroid.show('Enregistrement effectué avec succès !', ToastAndroid.LONG);
        }).then((response) => {
            console.log(JSON.stringify(response, null, 4));
            this._doDeleteAll('endingUpload');
            setTimeout(function () {
                this.props.navigation.navigate('Residence', {isEndingUpload: true});
            }.bind(this), 500);
        }).catch((error) => {
            console.log('Error :', JSON.stringify(error, null, 4));
            Alert.alert('Erreur', 'Veuillez vérifier votre connexion Internet.', [{
                text: 'Ok'
            }]);
            this.setState({ isLoading: false });
        });
    }

    render() {
        const residence = this.props.residence;
        return (
            <View style={styles.mainContainer}>
                <View style={styles.listContainer}>
                    <View style={stylesItem.listContainer}>
                        <TouchableOpacity style={stylesItem.subspace} onPress={this._loadPresentList}>
                            <Text style={stylesItem.subspaceName}>Liste des présents</Text>
                            <View style={stylesItem.subspaceIconContainer}>
                                <Icon name="chevron-right" size={40} color="#000" />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{residence.nom}</Text>
                </View>

                <FlatList style={styles.listContainer}
                          data={residence.espaces}
                          keyExtractor={(item) => item.nom.toString()}
                          renderItem={
                              ({item}) => <ResidenceInfoItem
                                  space={item}
                                  loadSubspace={this._loadSubspace}
                              />
                          }
                />
                <TouchableOpacity style={styles.buttonEndVisit} onPress={this._endingVisit.bind(this)}>
                    <Text style={styles.textEndVisit}>Terminer la visite</Text>
                </TouchableOpacity>
                {this._displayLoading()}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.tokenManagement.token,
        residence: state.residenceManagement.residence,
        picturesAndComment: state.subspaceManagement.picturesAndComment
    };
};

export default connect(mapStateToProps)(ResidenceInfo);