
const apiUrl = 'https://immodt.nanokwant.com/api';

export function getUserToken(mail, pass) {
    const url = apiUrl + '/auth';
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }, body: JSON.stringify({
            email: mail,
            password: pass
        })
    };

    return fetch(url, request)
        .then((response) => response.json());
}

export function getResidences(token) {
    const url = apiUrl + '/residences/' + token._id;
    const request = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.accessToken
        }
    };
    // TODO FAIRE MIEUX !!! (Comme plus haut)
    return fetch(url, request)
        .then((response) =>
            response.json()
        )
        .catch((error) => {
            console.error('Erreur API-GR-01:', error);
        });
}

export function postVisit(token, data) {
    const url = apiUrl + '/visites/';
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.accessToken
        },
        body: JSON.stringify(data)
    };

    return fetch(url, request)
        .then((response) => response.json());
}

export function postPhoto(token, data) {
    const url = apiUrl + '/uploads/photos';
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + token.accessToken,
            'residenceId': data.idResidence,
            'espaceId': data.idSubspace
        },
        body: data.value
    };
    //console.warn(JSON.stringify(request, null, 4));
    return fetch(url, request)
        .then(response => response.text());
}
/*
export function patchResidence(token, data) {
    const url = apiUrl + '/residences/cs/' + data._id;
    let body = data.membresCS;
    body.forEach((item, i) => {
        let expr = new RegExp("^[0-9a-z]$", "i");
        if (expr.test(item._id)) {
            delete item._id;
        }
    });
    const request = {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.accessToken
        },
        body: JSON.stringify(body)
    };
    //console.warn(body);
    return fetch(url, request)
        .then(response => response.text());
}*/