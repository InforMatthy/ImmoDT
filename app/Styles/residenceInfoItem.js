import {StyleSheet} from "react-native";
import React from "react";

const styles = StyleSheet.create({
    spaceTitleContainer: {
        borderBottomWidth: 1,
        borderBottomColor: "#bfbfbf",
        paddingBottom: 10,
        paddingLeft: 10,
        paddingTop: 5
    },
    spaceTitle: {
        fontSize: 16,
        color: "rgba(0, 51, 153, 0.5)"
    },
    listContainer: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10
    },
    subspace: {
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center'
    },
    subspaceName: {
        flex: 3,
        fontSize: 20,
        paddingBottom: 5
    },
    subspaceIconContainer: {
        flex: 1,
        borderLeftWidth: 0.5,
        borderLeftColor: "rgba(191, 191, 191, 0.4)"
    }
});

export default styles;