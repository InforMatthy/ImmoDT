import {FlatList, StyleSheet, View} from "react-native";
import React from "react";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        fontFamily: 'Roboto'
    },
    titleContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#ccc',
        paddingTop: 12,
        paddingBottom: 10,
        paddingLeft: 15,
        backgroundColor: "rgba(0, 51, 153, 0.5)"
    },
    title: {
        color: "#FFF",
        fontSize: 20,
        fontWeight: 'bold'
    },
    listContainer: {
        paddingTop: 20,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5
    },
    buttonEndVisit: {
        backgroundColor: 'rgb(230, 0, 0)',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    textEndVisit: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    loadingContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default styles;