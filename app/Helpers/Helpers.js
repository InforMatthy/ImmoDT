export default class Helpers {

    /**
     * Convert a date in json in date for display
     * in format DD/MM/YYYY
     * @param date {string}
     * @returns {string}
     */
    static convertDate(date) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        let d = new Date(date);
        return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
    }

    /**
     * Replace first character to lower case
     * @param a {string}
     * @returns {string}
     */
    static strLcFirst(a) {
        return (a).charAt(0).toLowerCase() + a.substr(1);
    }
}