import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    titleContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#ccc',
        paddingTop: 12,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: "rgba(0, 51, 153, 0.5)",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: 'bold'
    },
    secondContainer: {
        paddingTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5
    },
    inputGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: 'rgba(0, 0, 0, 0.4)',
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 20
    },
    inputContainer: {
        flex: 1
    },
    textInputGroup: {
        borderWidth: 0.5,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        borderRadius: 10,
        paddingTop: 8,
        paddingLeft: 8,
        marginBottom: 5,
        marginRight: 10
    },
    textInput: {
        width: '80%',
        marginBottom: 5
    },
    validateOption: {
        backgroundColor: 'rgb(0, 230, 0)',
        borderRadius: 5,
        padding: 5
    },
    inputContainerValidate: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: 'rgba(0, 0, 0, 0.4)',
        paddingTop: 5,
        paddingBottom: 5,
        marginBottom: 20
    },
    textInputValidate: {
        flex: 1,
        color: '#000'
    },
    deleteOption: {
        backgroundColor: 'rgb(230, 0, 0)',
        borderRadius: 5,
        padding: 5
    }
});

export default styles;