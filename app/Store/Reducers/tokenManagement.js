const initialState = { token: {} };

function tokenManagement(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'ADD_TOKEN':
            nextState = {
                ...state,
                token: action.value.token
            };
            return nextState || state;
        default:
            return state;
    }
}

export default tokenManagement;