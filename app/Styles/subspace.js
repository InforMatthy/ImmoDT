import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    titleContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#ccc',
        paddingTop: 12,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: "rgba(0, 51, 153, 0.5)",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: 'bold'
    },
    secondContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5
    },
    photoContainer: {
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: "#bfbfbf",
    },
    photoTile: {
        width: '33%',
        height: 120,
        margin: 1
    },
    photoTileImg: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover'
    },
    commentContainer: {
        flex: 1,
        paddingTop: 5,
        paddingBottom: 10
    },
    commentTitleContainer: {
        position: 'relative',
        top: 10,
        left: 10,
        backgroundColor: 'white',
        zIndex: 1000,
        width: '40%',
        alignItems: 'center'
    },
    commentTitle: {
        fontSize: 16,
        color: "rgba(0, 51, 153, 0.5)"
    },
    textInput: {
        borderWidth: 1,
        borderColor: "#bfbfbf",
        borderRadius: 2
    },
    buttonOk: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    buttonKo: {
        backgroundColor: 'rgb(230, 0, 0)',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    buttonText: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    }
});

export default styles;