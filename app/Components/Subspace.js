import React from 'react';
import {Alert, FlatList, Image, Text, TextInput, ToastAndroid, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import { Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import styles from '../Styles/subspace';
import {HeaderBackButton} from "react-navigation-stack";

class Subspace extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isUploaded: false,
            idSubspace: this.props.navigation.state.params.subspace._id,
            pictures: [],
            comment: '',
            isSave: true
        };
        this._imageClicked = this._imageClicked.bind(this);
    }

    componentDidMount() {
        if (this.props.picturesAndComment.findIndex(item => item.idSubspace === this.state.idSubspace) !== -1) {
            const {pictures, comment} = this.props.picturesAndComment.find(item => item.idSubspace === this.state.idSubspace);
            this.setState({
                pictures: pictures,
                comment: comment
            }, () => {
                if (this.state.pictures[0] && this.state.pictures[0].uri) {
                    this.setState({
                        isUploaded: true
                    });
                }
            });
        }
    }

    _imageClicked() {
        const options = {
            title: 'Ajouter une photo',
            takePhotoButtonTitle: 'Prendre une photo...',
            chooseFromLibraryButtonTitle: 'Ajouter depuis la galerie...',
            cancelButtonTitle: 'Annuler'
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('L\'utilisateur a annulé');
            }  else if (response.error) {
                console.log('Erreur : ', response.error);
            } else {
                console.log('Photo : ', response.uri );
                let requireSource = {
                    name: response.fileName,
                    uri: response.uri
                };
                this.state.isUploaded ? this.setState({
                    pictures: [...this.state.pictures, requireSource],
                    isSave: false
                }) : this.setState({
                    pictures: [requireSource],
                    isUploaded: true,
                    isSave: false
                });
            }
        });
    }

    _deleteImg(itemSearch) {
        Alert.alert(
            'Supprimer une image',
            'Voulez-vous vraiment supprimer cette image ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        let pictures = this.state.pictures;
                        pictures = pictures.filter(item => item.uri !== itemSearch.uri);
                        let isPictures = !!pictures[0];
                        this.setState({
                            pictures: isPictures ? pictures : [],
                            isUploaded: isPictures,
                            isSave: false
                        });
                        ToastAndroid.show('Image supprimé avec succès !', ToastAndroid.SHORT);
                    }
                },
                {
                    text: 'Annuler',
                    style: 'cancel',
                }
            ]
        );
    };

    _displayImage = ({item}) => {
        let picture = {uri: item.uri};
        return(
            <TouchableOpacity style={styles.photoTile} onPress={() => this._deleteImg(item)}>
                <Image
                    style={styles.photoTileImg}
                    source={picture}
                />
            </TouchableOpacity>
        );
    };

    _saveAllData = () => {
        const action = {
            type: "ADD_PICTURES_AND_COMMENT",
            value: {
                idSubspace: this.state.idSubspace,
                pictures: this.state.pictures,
                comment: this.state.comment
            }
        };
        this.props.dispatch(action);
        this.setState({isSave: true});
        ToastAndroid.show('Information de l\'espace sauvegardé avec succès !', ToastAndroid.LONG);
    };

    render() {
        const { subspace, residence, spaceName } = this.props.navigation.state.params;
        return (
            <KeyboardAwareScrollView>
                <View style={styles.mainContainer}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>
                            {residence.nom} / {spaceName} /
                            {"\n"}{subspace.nom}
                        </Text>
                        <TouchableOpacity onPress={this._imageClicked}>
                            <Icon name='add-a-photo' size={30} color='#FFF' />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.secondContainer}>
                        <View style={styles.photoContainer}>
                            <FlatList
                                data={this.state.pictures}
                                horizontal={false}
                                numColumns={3}
                                keyExtractor={item => item.id}
                                renderItem={this._displayImage}
                            />
                        </View>
                        <View style={styles.commentContainer}>
                            <View style={styles.commentTitleContainer}>
                                <Text style={styles.commentTitle}>Commentaire :</Text>
                            </View>
                            <TextInput style={styles.textInput}
                                       multiline={true}
                                       numberOfLines={10}
                                       onChangeText={text => this.setState({ comment: text, isSave: false})}
                                       value={this.state.comment}
                            />
                        </View>
                        <TouchableOpacity style={this.state.isSave ? styles.buttonOk : styles.buttonKo}
                                          onPress={this._saveAllData}
                                          disabled={this.state.isSave}
                        >
                            <Text style={styles.buttonText}>Enregistrer</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        picturesAndComment: state.subspaceManagement.picturesAndComment
    };
};

export default connect(mapStateToProps)(Subspace);