import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import moment from 'moment'

import styles from '../Styles/residenceItem';

class ResidenceItem extends React.Component {

    render() {
        const { residence, confirmForVisiting, isReloaded } = this.props;
        // Use Moment.js for display date
        const derniereVisite = moment(new Date(residence.derniereVisite)).format('DD/MM/YYYY')
        return (
            <TouchableOpacity style={styles.mainContainer} onPress={() => confirmForVisiting(residence, isReloaded)}>
                <Text style={styles.title}>{residence.nom}</Text>
                <Text style={styles.address}>{residence.adresse}</Text>
                <Text style={styles.address}>{residence.codePostal} {residence.ville}</Text>
                <Text style={styles.lastVisit}>Date de dernière visite : {derniereVisite}</Text>
            </TouchableOpacity>
        );
    }
}

export default ResidenceItem;