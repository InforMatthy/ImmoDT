import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginLeft: 25,
        marginRight: 25,
        fontFamily: 'Roboto',
        backgroundColor: 'white'
    },
    logoContainer: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 30
    },
    logo: {
        marginTop: 30,
        width: '80%',
        resizeMode: 'contain'
    },
    titleContainer: {
        flex: 1,
        marginTop: 30,
        alignItems: 'center'
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    inputContainer: {
        flex: 3
    },
    inputText: {
        fontSize: 15
    },
    inputSection: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: '#000',
        paddingTop: 5,
        paddingBottom: 5,
        marginBottom: 40
    },
    textInput: {
        flex: 1
    },
    button: {
        backgroundColor: 'rgb(230, 0, 0)',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 2
    },
    buttonText: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    loadingContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default styles;